exports.notFound = function notFound(req, res, next){
    res.send(404, '404 Error | No one here, just you and me :D');
    };

exports.error = function error(err, req, res, next){
    console.log(err);
    res.send(500, '500 | You broke something >:(');
    };