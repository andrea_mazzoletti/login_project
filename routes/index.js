//module.exports.index = index;
//module.exports.login = login;
//module.exports.loginProcess = loginProcess;
//module.exports.chat = chat;
var util = require('../middleware/utilities');
var config = require('../config');

exports.index = function index(req, res) {
    res.cookie('IndexCookie', 'This was set from Index');
    res.render('index', {
        layout: 'layout',
        title: 'index',
        body: 'index'
    })
};

exports.login = function login(req, res) {
    res.render('login', { title: 'Login', message: req.flash('error')});
};

exports.loginProcess = function loginProcess(req, res) {
    var isAuth = util.auth(req.body.username, req.body.password, req.
        session);
    if (isAuth) {
        res.redirect('/chat');
    } else {
        req.flash('error', 'Wrong Username or Password');
        res.redirect(config.routes.login);
    }
};


module.exports.chat = function chat(req, res) {
    res.render('chat', { title: 'Chat' });
};

exports.logOut =  function logOut(req, res) {
    util.logOut(req.session);
    res.redirect('/login');
};